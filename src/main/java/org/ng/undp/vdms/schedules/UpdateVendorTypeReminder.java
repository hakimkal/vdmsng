package org.ng.undp.vdms.schedules;

import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.services.SmtpService;
import org.ng.undp.vdms.services.UserService;
import org.ng.undp.vdms.services.VendorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;

import java.util.List;

/**
 * Created by abdulhakim on 2/8/18.
 */

@Component
public class UpdateVendorTypeReminder {

    private static Logger LOGGER = LoggerFactory.getLogger(UpdateVendorTypeReminder.class);

    @Autowired
    private VendorService vendorService;

    @Autowired
    private UserService userService;

    @Autowired
    private SmtpService smtpService;


    @Scheduled(cron = "0 0 11 * * SUN",zone = "Africa/Lagos")
    public void vendorprofileUpdateNotificationReminder() {
        LOGGER.info("Background job for vendor reminder running");

        List<User> vendorList = userService.getAllVendors();
        LOGGER.info("Collection Sized " + Integer.toString(vendorList.size()));

        for (User v : vendorList) {

            if (null == v.getDeletedAt() && v.getRoleNames().size() == 1) {

                for (String t : v.getRoleNames()) {

                    if (t.equals("VENDOR")) {
                       // LOGGER.info(v.getEmail());
                        sendVendorProfileUpdateNotificationReminder(v.getLastname(), v.getEmail());
                       // sendVendorProfileUpdateNotificationReminder(v.getLastname(), "hakimkal@gmail.com");
                    }
                }
            }

        }
    }


    @Async
    private void sendVendorProfileUpdateNotificationReminder(String lastname, String email) {
        // String homeURL = ServletUriComponentsBuilder.fromCurrentContextPath().path("/").build().toUriString();
        // String loginURL = ServletUriComponentsBuilder.fromCurrentContextPath().path("/accounts/login").build().toUriString();
        String SERVER_URL = "http://rosters.ng.undp.org";

        Context messageContext = new Context();
        messageContext.setVariable("userName", lastname);
        messageContext.setVariable("loginURL", SERVER_URL + "/accounts/login");
        messageContext.setVariable("homeURL", SERVER_URL);

        String messageBody = smtpService.prepareThymeleafMailBody("vendor-reminder", messageContext);
        LOGGER.info("Background job send vendor reminder email");

        smtpService.sendSmtpAsync(email, " Login to VDMS and update your profile ", messageBody, "", "");


    }
}
