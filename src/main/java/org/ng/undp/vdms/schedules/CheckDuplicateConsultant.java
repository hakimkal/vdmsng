package org.ng.undp.vdms.schedules;

import org.ng.undp.vdms.domains.Consultant;
import org.ng.undp.vdms.services.ConsultantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by abdulhakim on 3/18/18.
 */
@Component

public class CheckDuplicateConsultant {

    @Autowired
    ConsultantService consultantService;
    private static Logger LOGGER = LoggerFactory.getLogger(CheckDuplicateConsultant.class);

    @Scheduled(cron = "0 0 0  * * *", zone = "Africa/Lagos")
    public void checkConsultant() {
        LOGGER.info("Background job for consultant duplicate removal running");


        Iterable<Consultant> supplierCompanyList = consultantService.findAll();

        for (Consultant s : supplierCompanyList) {

            List<Consultant> consultants = consultantService.findAllByUser(s.getUser());

            if (consultants.size() > 1) {

                consultants.remove(0);
                deleteConsultant(consultants);

            }

        }


    }

    @Async
    public void deleteConsultant(List<Consultant> consultants) {
        for (Consultant c : consultants) {

            LOGGER.info("Deleting consultant duplicate  :: " + c.getName() + " ID: " + c.getId().toString());
            consultantService.deleteConsultant(c.getId());

        }


    }
}
