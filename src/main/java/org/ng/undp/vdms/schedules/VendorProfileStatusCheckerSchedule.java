package org.ng.undp.vdms.schedules;

import org.ng.undp.vdms.constants.SupplierDocumentType;
import org.ng.undp.vdms.domains.SupplierDocument;
import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.domains.Vendor;
import org.ng.undp.vdms.services.SmtpService;
import org.ng.undp.vdms.services.SupplierDocumentService;
import org.ng.undp.vdms.services.UserService;
import org.ng.undp.vdms.services.VendorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;

import java.util.List;

/**
 * Created by abdulhakim.
 */
@Component
public class VendorProfileStatusCheckerSchedule {

    private static Logger LOGGER = LoggerFactory.getLogger(VendorProfileStatusCheckerSchedule.class);

    @Autowired
    private VendorService vendorService;

    @Autowired
    private UserService userService;


    @Autowired
    private SupplierDocumentService supplierDocumentService;


    //Every hour
    @Scheduled(cron = "0 0 6 * * *", zone = "Africa/Lagos")
    public void supplierProfileUChecker() {
        LOGGER.info("Background job to check supplier profile status ");

        List<User> vendorList = userService.getAllSuppliers();

        for (User v : vendorList) {

            SupplierDocument reg = supplierDocumentService.findOneByUserAndSupplierDocumentType(v, SupplierDocumentType.REGISTRATION_CERTIFICATE);
            SupplierDocument fin = supplierDocumentService.findOneByUserAndSupplierDocumentType(v, SupplierDocumentType.FINANCIAL_REPORT);
            SupplierDocument dir = supplierDocumentService.findOneByUserAndSupplierDocumentType(v, SupplierDocumentType.DIRECTORS_FORM);

            if (null != reg || null != fin || null != dir) {

                updateStatus(vendorService.findOneByUser(v), true);

            }

        }


    }

    //Every 7am
    @Scheduled(cron = "0 0 7 * * *", zone = "Africa/Lagos")
    public void ngoProfileUChecker() {
        LOGGER.info("Background job to check ngo profile status ");

        //List<User> vendorList = userService.getAllSuppliers();

        //for (User v : vendorList) {

                  // updateStatus(vendorService.findOneByUser(v), true);



        //}


    }

    //Every 5am
    @Scheduled(cron = "0 0 5 * * *", zone = "Africa/Lagos")
    public void consultantProfileUChecker() {
        LOGGER.info("Background job to check consultant profile status ");

        //List<User> vendorList = userService.getAllSuppliers();

        //for (User v : vendorList) {

        // updateStatus(vendorService.findOneByUser(v), true);



        //}


    }



    @Async
    void updateStatus(Vendor vendor, boolean status) {

        vendor.setProfileCompleted(status);
        vendorService.save(vendor);


    }
}
