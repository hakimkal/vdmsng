package org.ng.undp.vdms.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * Created by abdulhakim on 12/6/16.
 */


@Data
@Entity
@Table(name="countries")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Country  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "id")

    private Long id;

    @OneToMany
    @JsonIgnore
    public List<State> states;
    public String name;

    public String nativeName;


    public String[] callingCode;

    public String capital;




    public  Country(){}
    public void setName(String name){
        if(name != null){
            this.name =name.toUpperCase();
        }
    }
}
