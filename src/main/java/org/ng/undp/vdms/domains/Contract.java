package org.ng.undp.vdms.domains;

import lombok.Data;
import org.ng.undp.vdms.domains.constants.Status;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by emmanuel on 6/10/17.
 */
@Data
@Entity
@Table(name = "contracts")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @Column(columnDefinition = "text")
    private String details;

    @ManyToOne
    @JoinColumn(name = "vendor_id")
    private Vendor vendor;


    @Transient
    private Long vendorId;


   /* public Long getVendorId(){

   if(null != this.getVendor()){
        return this.getVendor().getId();}
        return 0L;

    }*/

    @Temporal(TemporalType.TIMESTAMP)
    private Date contractDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date contractExpiryDate;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "contract")
    private List<ContractDocument> contractDocuments;

    private boolean reminderSent = false;
    private int reminderSentCount = 0;
    @Temporal(TemporalType.TIMESTAMP)
    private Date emailSentDate;

    private Status status;

    private BigDecimal amount;

    @ElementCollection
    private List<String> tags;

    @ManyToOne
    @JoinColumn(name = "assigned_to_id")
    private User assignedTo;

    @ManyToOne
    private User statusChangedBy;

    private Date statusChangedAt;

    private Date created_at;

    private Date updated_at;


    @PrePersist
    void createdAt() {
        this.created_at = this.updated_at = new Date();
    }

    @PreUpdate
    void updatedAt() {
        this.updated_at = new Date();
    }

}
