package org.ng.undp.vdms.domains.trackers;


import lombok.Data;

import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.domains.constants.Env;
import org.ng.undp.vdms.interfaces.Synchronizable;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by emmanuel on 7/12/17.
 */
@Data
@Entity
@Table(name = "login_history")
public class LoginHistory implements Synchronizable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid", nullable = false, unique = true)
    private String uuid;

    @NotNull
    @ManyToOne
    private User user;

    private  String macAddress;
    private  String pcName;
    private  String os;

    @Column
    private LocalDateTime loginTime;

    @Column
    private LocalDateTime LogoutTime;

    @Enumerated(EnumType.STRING)
    private Env env;

    private String ip;


    private boolean synched;//has this Login Record being synched

    private boolean isDirty;//field used to mark an existing entity eligible for Synchronization

    @Column(insertable = false, updatable = false)
    private Boolean synching;//Transient field to denote if an update is as a result of synching or a normal update

}
