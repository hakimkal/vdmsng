package org.ng.undp.vdms.domains;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.ng.undp.vdms.utils.ShortUUID;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by macbook on 5/1/17.
 */


@Entity
@Table(name = "vendors")
@Getter
@Setter
public class Vendor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @NotNull
    @Column(unique = true)
    private String uuid;

    private String name;

    @Transient
    private String vendorName;

    public String getVendorName() {


        return this.getUser().getFirstname() + " " + this.getUser().getLastname();

    }

    private boolean profileCompleted = false;

    @Transient
    private String vendorType;

    public String getVendorType() {

        String role = "NULL";
        Set<String> roles = this.getUser().getRoleNames();

        for (String r : roles) {
            if (!r.toUpperCase().equals("VENDOR")) {
                return r.toString().toUpperCase();

            }
        }


        return role;
    }

    @Cascade(org.hibernate.annotations.CascadeType.DETACH)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @OneToMany(mappedBy = "vendor")

    private List<VendorVpa> vpa;


    @Cascade(org.hibernate.annotations.CascadeType.DETACH)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @OneToMany(mappedBy = "vendor")


    private List<VendorVss> vss;

    @Cascade(org.hibernate.annotations.CascadeType.DETACH)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @OneToMany(mappedBy = "vendor")


    private List<VendorSkill> skill;


    //@JoinColumn(name="user_uuid")
    @OneToOne(cascade = CascadeType.ALL)

    private org.ng.undp.vdms.domains.User user;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updatedAt;

    /*
     * This field is here to check whether a field is deleted or not.
     * by default, the field is NULL, if it carries a timestamp instead of null
     * then the record has been deleted,
     */

    @Column(name = "deleted_at")
    private Date deletedAt;

    private boolean suspended = false;

    public Vendor() {
    }

    public boolean hasVendorTypeStatusChanged() {
        if (this.getVendorType().equals("NULL")) {

            return false;

        }
        return true;

    }

    @PreUpdate
    void updatedAt() {
        this.updatedAt = new Date();
    }


    @PrePersist
    void createAt() {

        this.setProfileCompleted(false);

        this.createdAt = this.updatedAt = new Date();
        this.uuid = ShortUUID.shortUUID();


    }


}
