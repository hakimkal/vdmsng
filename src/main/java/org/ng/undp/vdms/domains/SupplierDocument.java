package org.ng.undp.vdms.domains;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.ng.undp.vdms.constants.SupplierDocumentType;
import org.ng.undp.vdms.utils.ShortUUID;
import org.ng.undp.vdms.utils.Utility;

import javax.persistence.*;
import java.util.Date;

/**
 * Author: Kingsley Eze.
 * Project: vdms
 * Date: 6/16/2017.
 */

@Entity
@Table(name = "supplier_documents")
@Data
public class SupplierDocument extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated(EnumType.STRING)
    @Column
    private SupplierDocumentType supplierDocumentType;

    private String name;

    private String fileName;




}
