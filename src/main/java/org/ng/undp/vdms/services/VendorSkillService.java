package org.ng.undp.vdms.services;

import org.ng.undp.vdms.domains.Skill;
import org.ng.undp.vdms.domains.Vendor;
import org.ng.undp.vdms.domains.VendorSkill;
import org.ng.undp.vdms.repositories.VendorSkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by abdulhakim on 7/16/17.
 */
@Service
public class VendorSkillService {

    @Autowired
    private VendorSkillRepository vendorSkillRepository;


    public Iterable<VendorSkill> findAll() {

        return vendorSkillRepository.findAll();
    }



    public VendorSkill getVpa(Long id) {
        return vendorSkillRepository.findOne(id);
    }
    public VendorSkill findOneByVendorAndSkill(Vendor v, Skill s) {
        return vendorSkillRepository.findOneByVendorAndSkill(v,s);
    }

    public VendorSkill editVpa(VendorSkill vpa) {
        return vendorSkillRepository.save(vpa);
    }

    public void delete(Long id) {
        vendorSkillRepository.delete(id);
    }
    public VendorSkill save(VendorSkill d) {
        return vendorSkillRepository.save(d);
    }

    public List<VendorSkill> findAllById(List<Long> vpaIds){return  vendorSkillRepository.findAllById(vpaIds);}


}
