package org.ng.undp.vdms.services;

import org.ng.undp.vdms.domains.Vendor;
import org.ng.undp.vdms.domains.VendorVpa;
import org.ng.undp.vdms.domains.Vpa;
import org.ng.undp.vdms.domains.constants.UserType;
import org.ng.undp.vdms.repositories.VendorVpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by abdulhakim on 7/16/17.
 */
@Service
public class VendorVpaService {

    @Autowired
    private VendorVpaRepository vendorVpaRepository;

    public Iterable<VendorVpa> findAll() {

        return vendorVpaRepository.findAll();
    }



    public VendorVpa getVpa(Long id) {
        return vendorVpaRepository.findOne(id);
    }
    public VendorVpa findOneByVendorAndVpa(Vendor v, Vpa id) {
        return vendorVpaRepository.findOneByVendorAndVpa(v,id);
    }

    public VendorVpa editVpa(VendorVpa vpa) {
        return vendorVpaRepository.save(vpa);
    }

    public void delete(Long id) {
        vendorVpaRepository.delete(id);
    }
    public VendorVpa save(VendorVpa d) {
        return vendorVpaRepository.save(d);
    }

    public  List<VendorVpa> findAllById(List<Long> vpaIds){return  vendorVpaRepository.findAllById(vpaIds);}


}
