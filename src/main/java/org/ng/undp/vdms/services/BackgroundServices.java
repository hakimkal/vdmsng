package org.ng.undp.vdms.services;


import org.ng.undp.vdms.constants.SupplierDocumentType;
import org.ng.undp.vdms.dao.Accessor;
import org.ng.undp.vdms.dao.Filter;
import org.ng.undp.vdms.domains.Notice;
import org.ng.undp.vdms.domains.SupplierDocument;
import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.domains.Vendor;
import org.ng.undp.vdms.domains.security.Role;
import org.ng.undp.vdms.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;


/**
 * Created by macbook on 7/1/17.
 */

@Service
public class BackgroundServices {
    private static Logger logger = LoggerFactory.getLogger(BackgroundServices.class);

    @Autowired
    private VendorService vendorService;

    @Autowired
    private SmtpService smtpService;
    @Autowired
    private NoticeService noticeService;

    @Async
    public void newNoticeAlertBlaster(Role userType, List<Notice> notice) {
        logger.info("new Notice alert {}", notice);
        try {



            List<Vendor> vendorList =  vendorService.findAllActiveVendorUserAndUserType(userType.getId());


            for (Vendor v : vendorList) {

                if (v.getUser().getRoleNames().contains(userType.getName())) {
                    newNoticeAlertMail(v, notice);
                }


            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Async
    public void allNoticeAlertBlaster(List<Notice> notice) {

        try {

            // Set<Role> roleSet = Accessor.findList(Vendor.class, Filter.get().field("user.roles").contains(userType.getId()).field("user.deleted_at").isNull());
            List<Set<Role>> roles = new ArrayList<Set<Role>>();

            Set<Long> roleSet = new HashSet<Long>();
            List<Vendor> vendorList = new ArrayList<>();
            notice.forEach(r -> {
                roles.add(r.getRoles());

            });
            roles.forEach(r -> {

                r.forEach(p -> {
                    roleSet.add(p.getId());


                });
            });
            roleSet.forEach(p -> {

                vendorList.addAll(
                      //  Accessor.findList(Vendor.class, Filter.get().field("suspended", false).field("deletedAt").isNull().field("user.roles").contains(p).field("user.deleted_at").isNull())
                vendorService.findAllActiveVendorUserAndUserType(p)
                );

            });


            for (Vendor v : vendorList) {


                newNoticeAlertMail(v, notice);


            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Scheduled(cron = "0 0 0/12 * * ?")
    //Check Notice Active Status Calendars annually, Run every two-hours in the month of August, every year

    //@Scheduled(cron = "0/5 * * * * ?") //5 seconds
    public void toggleNoticeActiveStatus() {
        logger.info("Cron task running for notices every 12 Hour");
        List<Notice> notices = Accessor.findList(Notice.class, Filter.get().field("active", true));


        for (Notice n : notices) {
            boolean datePAST = this.isDatePast(n.getClosingDate());
            // System.out.println("THE DATE IS PAST? " + datePAST);

            if (!datePAST) {
                this.updateNoticeStatus(n);

            }
        }

    }


    @Scheduled(cron = "0 0 0/12 * * ?")
    // @Scheduled(cron = "*/5 * * * * ?")
    //@Scheduled(cron = "0 15 10 15 * ?")  10:15am of the 15th day of every month
    public void checkSupplierFinancialDocumentExpiration() {
        logger.info("CRON TASK FOR SUPPLIER FINANCIAL EXPIRY");
        List<SupplierDocument> supplierDocuments = Accessor.findList(SupplierDocument.class, Filter.get().field("supplierDocumentType", SupplierDocumentType.FINANCIAL_REPORT));
        int year, thisYear = 0;
        LocalDate todayDate = LocalDate.now();

        thisYear = todayDate.getYear();
        int month, thisMonth = 0;

        thisMonth = todayDate.getMonthValue();


        String messageToSend = "";
        for (SupplierDocument s : supplierDocuments) {

            LocalDate docCreatedDate = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(s.getCreated_at()));

            year = docCreatedDate.getYear();
            month = docCreatedDate.getMonthValue();

            logger.info("This month is  " + thisMonth + " " + s.getFileName() + "\n");
            logger.info("The doc month is  " + month + " " + s.getFileName() + "\n");
            logger.info("The Year of the document  " + Integer.toString(year) + "\n");


            if ((thisYear - year >= 1) && (thisMonth >= month)) {

                ///this.sendReminder(s.getUser(), "Expired Financial Document", messageToSend);
            }
        }

    }


    @Scheduled(cron = "0 0 0/12 * * ?")
    public void checkNGOFinancialDocumentExpiration() {
        logger.info("CRON TASK FOR NGO FINANCIAL EXPIRY");

    }

    private void checkVendorContractExpiration() {

    }

    private void checkVendorAvailability() {

    }


    @Scheduled(cron = "0 0 0/12 * * ?")
    @Async
    public void checkVendorLastLogin() {
        logger.info("CRON TASK FOR Check Vendor Last Login ");
    }


    private void newNoticeAlertMail(Vendor v, List<Notice> notice) {
        logger.info("new Notice Email to send {}", notice);

        Context messageContext = new Context();


        messageContext.setVariable("userName", v.getUser().getLastname());
        messageContext.setVariable("notices", notice);

        String messageBody =
                smtpService.prepareThymeleafMailBody("new-notice-alert", messageContext);

        smtpService.sendSmtpAsync(v.getUser().getEmail(), "New Notice Alert from VDMS ", messageBody, "", "");

       // smtpService.sendSmtpAsync("breachandbeyond@gmail.com", "New Notice Alert from VDMS ", messageBody, "", "");

    }

    @Async
    public void sendReminder(User v, String subject, String message) {

        Context messageContext = new Context();


        messageContext.setVariable("userName", v.getLastname());
        messageContext.setVariable("message", message);

        String messageBody =
                smtpService.prepareThymeleafMailBody("new-notice-alert", messageContext);

        smtpService.sendSmtpAsync(v.getEmail(), subject, messageBody, "", "");


    }


    @Async
    public void updateNoticeStatus(Notice n) {
        n.setActive(false);
        noticeService.save(n);
    }

    @Async
    public void changeNoticeSendStatus(Notice notice) {

        logger.info("Notice Sent Status Change from for asynchroniously..." + notice.getPosition());
        notice.setAlertSent(true);
        notice.setNotifiedVendors(true);
        notice.setLastNotificationSendDate(new Date());
        noticeService.save(notice);


    }

    private boolean isDatePast(Date theDate) {
        return DateUtils.isPastDateFromToday(theDate);
    }

}

