package org.ng.undp.vdms.controllers;


import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.domains.trackers.LoginHistory;
import org.ng.undp.vdms.services.LoginHistoryService;
import org.ng.undp.vdms.utils.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Created by abdulhakim on 7/12/17.
 */

@Controller
@RequestMapping(value = "logins")
public class LoginController extends BaseController {
    @Autowired
    LoginHistoryService loginHistoryService;


    @PreAuthorize("hasAnyAuthority('ADMIN_ACCOUNT','VENDOR','SUPPLIER','CONSULTANT','NGO','STAFF','UNSTAFF')")

    @GetMapping(value = "audit")

    public String getUserLogins(HttpServletRequest request, Pageable pageable, Model model, Principal principal) {
        User AUTH_USER = Auth.INSTANCE.getAuth().get();
        Page<LoginHistory> list = loginHistoryService.findAllByUser(pageable, AUTH_USER);
        model.addAttribute("logins", list);


        if (list.hasPrevious()) {
            model.addAttribute("prev", pageable.previousOrFirst());
        }
        if (list.hasNext()) {
            model.addAttribute("next", pageable.next());
        }


        return "logins/all";
    }

    @PreAuthorize("hasAnyAuthority('ADMIN_ACCOUNT', 'STAFF','UNSTAFF')")

    @GetMapping(value = "audits")

    public String getUsersLogins(Pageable pageable, HttpServletRequest request, Model model, Principal principal) {

        Page<LoginHistory> list = loginHistoryService.findAll(pageable);
        model.addAttribute("logins", list);


        if (list.hasPrevious()) {
            model.addAttribute("prev", pageable.previousOrFirst());
        }
        if (list.hasNext()) {
            model.addAttribute("next", pageable.next());
        }

        return "logins/all";
    }
}
