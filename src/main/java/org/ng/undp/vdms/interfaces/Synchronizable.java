package org.ng.undp.vdms.interfaces;

/**
 * Created by emmanuel on 5/5/17.
 */
public interface Synchronizable {
    void setSynched(boolean synched);//synched field is use to determine if the entity implementing it is a new entity not yet synchronized

    //void makeDirty();//isDirty field is use to determine if the entity implementing it is a old entity eligible for  synchronization update
    void setDirty(boolean dirty);

    void setSynching(Boolean synching);

    String getUuid();

}
