package org.ng.undp.vdms.configs;

/**
 * Created by macbook on 3/16/17.
 */


import org.ng.undp.vdms.domains.constants.Env;
import org.ng.undp.vdms.services.UserService;
import org.ng.undp.vdms.storage.StorageProperties;
import org.ng.undp.vdms.utils.NetworkConnectivityChecker;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;

public class NetWorkUtil {
    private Path rootLocation;
    private static String OS = System.getProperty("os.name").toLowerCase();
    @Autowired
    private static StorageProperties properties;


   @Autowired
    private static UserService userService;




/* Just incase you need the Public IP Address of the School Network*/

    public static String getIp() throws Exception {



        URL whatismyip = new URL("http://checkip.amazonaws.com");
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));
            String ip = in.readLine();
            return ip;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    //   e.printStackTrace();
                }
            }
        }
    }


    /* Return the local IP of the machine  */

    public static String getLocalIPAddress() {

        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                if (iface.isLoopback() || !iface.isUp() || iface.isVirtual() || iface.isPointToPoint())
                    continue;

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();

                    final String ip = addr.getHostAddress();
                    if (Inet4Address.class == addr.getClass()) return ip;
                }
            }
        } catch (SocketException e) {
            // throw new RuntimeException(e);
        }
        return null;
    }


    /* Return the local Mac Address */

    public static String getMacAddress() {

        InetAddress IP;
        StringBuilder sb = new StringBuilder();

        try {

            IP = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(IP);


            byte[] mac = network.getHardwareAddress();


            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }


        } catch (Exception e) {
            //  throw new RuntimeException(e);
        }
        return sb.toString();

    }
    /* Check if port 5000  is open or not*/

    public static boolean isPortInUse(String host) {
        // Assume no connection is possible.
        boolean result = false;
        int port = 5000;
        try {
            (new Socket(host, port)).close();
            result = true;
        } catch (SocketException e) {
            // Could not connect.
        } catch (UnknownHostException e) {
            //e.printStackTrace();

        } catch (IOException e) {
            // e.printStackTrace();
        }

        return result;
    }

    public static boolean isSchoolServer() {

        NetWorkUtil netWorkUtil = new NetWorkUtil();
        Path rootLocation = Paths.get("uploads");
        boolean result = false;
        try {

            if (Files.isDirectory(rootLocation.toAbsolutePath())) {
                System.out.println("The Upload directory path is : " + rootLocation.toAbsolutePath().toString());
                result = true;
            }


        } catch (Exception e) {

        }

        return result;
    }


    public static String getSchoolServerName() {

        String hostname = "Unknown";

        try {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        } catch (UnknownHostException ex) {
            //  System.out.println("Hostname can not be resolved");
        }
        return hostname;
    }

    public static String getOsName() {
        if (OS == null) {
            OS =  System.getProperty("os.name").toLowerCase();
        }

        return OS;
    }

    public static boolean isWindows() {

        return (OS.indexOf("win") >= 0);

    }

    public static boolean isMac() {

        return (OS.indexOf("mac") >= 0);

    }

    public static boolean isUnix() {

        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );

    }

    public static boolean isSolaris() {

        return (OS.indexOf("sunos") >= 0);

    }

    public String getRemoteMACAddress(String ip){
        String str = "";
        String macAddress = "";
        try {
            Process p = Runtime.getRuntime().exec("nbtstat -A " + ip);
            InputStreamReader ir = new InputStreamReader(p.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            for (int i = 1; i <100; i++) {
                str = input.readLine();
                if (str != null) {
                    if (str.indexOf("MAC Address") > 1) {
                        macAddress = str.substring(str.indexOf("MAC Address") + 14, str.length());
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
        return macAddress;
    }
}

