package org.ng.undp.vdms.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

/**
 * Author: Kingsley Eze.
 * Project: vdms
 * Date: 6/19/2017.
 */

@Data
public class SupplierFinancialDto {

    private Long id;

    @NotEmpty(message = "Please enter bank name")
    private String bankName;

    @NotEmpty(message = "Please enter account name")
    private String bankAccountName;

    @NotEmpty(message = "Please enter account number")
    private String bankAccountNumber;

    @NotEmpty(message = "Please enter address")
    private String bankAddress;


    private String swiftOrBicAddress;

    private String totalIncomeYearOne;

    private String totalIncomeYearTwo;

    private String totalIncomeYearThree;

    private String exportSalesYearOne;

    private String exportSalesYearTwo;

    private String exportSalesYearThree;

    private MultipartFile supplierDocument;
}
