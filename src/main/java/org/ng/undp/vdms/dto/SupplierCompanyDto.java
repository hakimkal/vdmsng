package org.ng.undp.vdms.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

/**
 * Author: Kingsley Eze.
 * Project: vdms
 * Date: 6/18/2017.
 */

@Data
public class SupplierCompanyDto {

    private Long id;

    @NotEmpty(message = "Please Enter Company Name")
    private String company;

    @NotEmpty(message = "Please Enter Street")
    private String street;

    @NotEmpty(message = "Please Enter Postal Code")
    private String postalCode;

    @NotEmpty(message = "Please select city")
    private String city;

    @NotEmpty(message = "Please Enter Country")
    private String country;

    @NotEmpty(message = "Please enter postal address")
    private String postalAddress;

    @NotEmpty(message = "Please enter telephone number")
    private String telephoneNumber;

    @NotEmpty(message = "Please enter email address")
    private String emailAddress;

    private String faxNumber;

    private String internetAddress;

    @NotEmpty(message = "Please enter contact name and title")
    private String contactNameAndTitle;

    @NotEmpty(message = "Please enter parent company")
    private String parentCompany;

    private MultipartFile subsidiaries;
    private MultipartFile registration;
    private MultipartFile directors;

    private String natureOfBusiness;

    private String natureOfBusinessOther;

    private String typeOfBusiness;

    private String typeOfBusinessOther;

    @NotEmpty(message = "Please enter year of establishment")
    private String yearEstablished; //Use calendar plugin to protect and maintain.

    @NotEmpty(message = "Please enter number of fill time employee")
    private String numberOfFullTimeEmployee;

    @NotEmpty(message = "Please enter state where registered")
    private String licenceNumberOrStateRegistered;

    @NotEmpty(message = "Please enter tax identification")
    private String vatNumberTaxIdentification;

    private MultipartFile supplierDocument;

    private String documentLanguage;

    private String documentLanguageOther;

    private String workingLanguage;

    private String workingLanguageOther;
}
