package org.ng.undp.vdms.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.ToString;
import org.ng.undp.vdms.domains.State;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
@ToString
public class CountriesDto {


        private Long id;


        @JsonIgnore
        public List<State> states;
        public String name;

        public String nativeName;

        public String[] tld;

        public String cca2;

        public String ccn3;

        public String cca3;

        public String[] currency;

        public String[] callingCode;

        public String capital;

        public String[] altSpellings;

        public String relevance;

        public String region;

        @ElementCollection
        @MapKeyColumn(name="name")
        @Column(name="value")
        @CollectionTable(name="country_translations", joinColumns=@JoinColumn(name="country_id"))

        public Map<String, String> translations;

        public String subregion;

        public String[] language;

        public String[] languageCodes;



        public Long[] latlng;

        public String demonym;

        public String[] borders;

        public Integer area;


    }

