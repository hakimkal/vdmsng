package org.ng.undp.vdms.repositories;

import org.ng.undp.vdms.domains.Consultant;
import org.ng.undp.vdms.domains.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by abdulhakim on 11/12/16.
 */
public interface ConsultantRepository  extends CrudRepository<Consultant, Long>, JpaSpecificationExecutor<Consultant> {

    public  Consultant findOne(Long id);

    public Consultant findByUserId(Long id);
    public List<Consultant> findAllByUser(User user);
    }
