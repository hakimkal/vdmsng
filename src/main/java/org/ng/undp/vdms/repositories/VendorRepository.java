package org.ng.undp.vdms.repositories;

import org.ng.undp.vdms.domains.User;
import org.ng.undp.vdms.domains.Vendor;
import org.ng.undp.vdms.domains.Vpa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Created by macbook on 5/1/17.
 */
public interface VendorRepository extends JpaRepository<Vendor, String>, JpaSpecificationExecutor<Vendor> {

    public Vendor findOneByUuid(String uuid);

    public Vendor findOneByUser(User uuid);

    public void deleteByUuid(String uuid);

    public Vendor findOneByid(Long id);

    public List<Vendor> findAll();

    public Page<Vendor> findAllByDeletedAtIsNullAndUserDeletedAtIsNull(Pageable pageable);

    public Page<Vendor> findAllByDeletedAtIsNullAndUserDeletedAtIsNullAndUserRolesId(Pageable pageable, Long vendorType);

    public Page<Vendor> findAllByDeletedAtIsNullAndUser_DeletedAtIsNullAndUser_RolesIdOrderByCreatedAtDesc(Pageable pageable, Long vendorType);

    public List<Vendor> findAllByVpa(Vpa vpa);

    public List<Vendor> findAllByUser(User user);

    public List<Vendor> findAllByUuid(List<String> uuid);


    Page<Vendor> findAllByDeletedAtIsNullAndUserDeletedAtIsNullAndUserRolesIdOrderByIdDesc(Pageable pageable, Long vendorType);

    List<Vendor> findAllByDeletedAtIsNullAndUserDeletedAtIsNullAndUserRolesIdOrderByIdDesc(Long vendorType);

    Page<Vendor> findByDeletedAtIsNullAndSuspendedFalseAndUser_RolesIdAndVpa_IdOrderByIdDesc(Long vendorType, int vpaId, Pageable pageable);

    Page<Vendor> findAllByDeletedAtIsNullAndSuspendedIsFalseAndVpaVpaIdOrderByIdDesc(Long vpaId, Pageable pageable);
}
