package org.ng.undp.vdms.repositories;

import org.ng.undp.vdms.constants.SupplierDocumentType;
import org.ng.undp.vdms.domains.SupplierDocument;
import org.ng.undp.vdms.domains.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author: Kingsley Eze.
 * Project: vdms
 * Date: 6/18/2017.
 */

@Repository
public interface SupplierDocumentRepository extends CrudRepository<SupplierDocument, Long>,JpaSpecificationExecutor<SupplierDocument> {
    public SupplierDocument findOneByUserAndSupplierDocumentType(User user, SupplierDocumentType supplierDocumentType);
    public List<SupplierDocument> findAllByUser(User user);
}
