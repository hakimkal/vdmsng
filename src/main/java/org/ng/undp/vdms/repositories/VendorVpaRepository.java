package org.ng.undp.vdms.repositories;

import org.ng.undp.vdms.domains.Vendor;
import org.ng.undp.vdms.domains.VendorVpa;
import org.ng.undp.vdms.domains.Vpa;
import org.ng.undp.vdms.domains.constants.UserType;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by abdulhakim on 7/16/17.
 */
public interface VendorVpaRepository   extends CrudRepository<VendorVpa, Long>, JpaSpecificationExecutor<VendorVpa> {

    public VendorVpa findOneByVendorAndVpa(Vendor v, Vpa id);




    @Query("SELECT q FROM VendorVpa q WHERE  q.id IN (:vpaIds)")
    public List<VendorVpa> findAllById(@Param("vpaIds") List<Long> vpaIds);

}
