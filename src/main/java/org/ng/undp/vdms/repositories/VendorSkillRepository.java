package org.ng.undp.vdms.repositories;

import org.ng.undp.vdms.domains.Skill;
import org.ng.undp.vdms.domains.Vendor;
import org.ng.undp.vdms.domains.VendorSkill;
import org.ng.undp.vdms.domains.VendorVpa;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by abdulhakim on 7/16/17.
 */
public interface VendorSkillRepository  extends CrudRepository<VendorSkill, Long>, JpaSpecificationExecutor<VendorSkill> {


    public VendorSkill findOneByVendorAndSkill(Vendor v, Skill s);
    @Query("SELECT q FROM VendorSkill q WHERE  q.id IN (:vpaIds)")
    public List<VendorSkill> findAllById(@Param("vpaIds") List<Long> vpaIds);

}
